from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.db import models


class Profile(models.Model):
    user = models.OneToOneField(get_user_model(), related_name='profile', on_delete=models.CASCADE,
                                verbose_name='User')
    birth_date = models.DateField(null=True, blank=True, verbose_name='birth date')
    avatar = models.ImageField(null=True, blank=True, upload_to='user_pics', verbose_name='avatar')
    position = models.ManyToManyField(
        Group,
        verbose_name='position',
        blank=True,
        help_text='Select user position'
    )
    phone_number = models.CharField(
        max_length=15,
        verbose_name='phone number',
        help_text='Enter your phone number in the format +7 (XXX) XXX-XX-XX',
        blank=True, null=True
    )

    def __str__(self):
        return self.user.get_full_name() + ' s profile'
# Create your models here.
