from django.contrib import admin
from .models import Profile
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Permission


class ProfileInline(admin.StackedInline):
    model = Profile
    fields = ['birth_date', 'avatar', 'phone_number', 'position']


class ProfileAdmin(UserAdmin):
    inlines = [ProfileInline]


User = get_user_model()
admin.site.unregister(User)
admin.site.register(User, ProfileAdmin)
admin.site.register(Permission)
# Register your models here.
