from django.urls import path
from .views import login_view, logout_view, RegisterView, UserDetailView, UserEditView, UserPasswordChangeView
from django.contrib.auth.views import LoginView, LogoutView

urlpatterns = [
    path('login/', login_view, name='login'),
    path('logout/', logout_view, name='logout'),
    path('register/', RegisterView.as_view(), name='register'),
    path('<int:pk>/', UserDetailView.as_view(), name='user_detail'),
    path('edit/', UserEditView.as_view(), name='user_edit'),
    path('password_change/', UserPasswordChangeView.as_view(), name='password_change'),

]
