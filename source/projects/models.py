from django.contrib.auth import get_user_model
from django.db import models


# Create your models here.

class Projects(models.Model):
    name = models.CharField(max_length=250, blank=False, null=False)
    description = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now=True)
    users = models.ManyToManyField(get_user_model(), related_name='users', verbose_name='users')

    def __str__(self):
        return self.name

    def to_dict(self):
        return {
            'title': self.name,
            'description': self.description,
            'created_at': self.created_at.isoformat(),
        }
