from django.urls import path
from .views import ProjectsListView, ProjectDetailView, \
    ProjectUpdateView, ProjectDeleteView, ProjectCreateView, ProjectUserUpdateView, error_page

urlpatterns = [
    path('list/', ProjectsListView.as_view(), name='projects_list'),
    path('detail/<int:pk>/', ProjectDetailView.as_view(), name='project_detail'),
    path('update/<int:pk>/', ProjectUpdateView.as_view(), name='project_update'),
    path('update/users/<int:pk>/', ProjectUserUpdateView.as_view(), name='project_user_update'),
    path('delete/<int:pk>/', ProjectDeleteView.as_view(), name='project_delete'),
    path('create/', ProjectCreateView.as_view(), name='project_create'),
    path('error/', error_page, name='error_page'),

]
