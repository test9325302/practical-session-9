from django import forms
from .models import Projects
from django.contrib.auth import get_user_model


class ProjectForm(forms.ModelForm):
    class Meta:
        model = Projects
        fields = ['name', 'description']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'description': forms.Textarea(attrs={'class': 'form-control'})
        }

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)


class ProjectUserEditForm(ProjectForm):
    class Meta(ProjectForm.Meta):
        # model = Projects
        fields = ['users']
        widgets = {'users': forms.SelectMultiple(attrs={'class': 'form-select'})}

