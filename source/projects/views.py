from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin
from django.contrib.auth.models import Permission
from django.core.exceptions import PermissionDenied
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, UpdateView, DeleteView, CreateView
from .models import Projects
from django.db.models import Q
from .forms import ProjectForm, ProjectUserEditForm
from django.contrib.auth.models import Group
from django.shortcuts import redirect
from django.contrib import messages


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Projects
    form_class = ProjectForm
    template_name = 'projects/project_create.html'
    success_url = reverse_lazy('projects_list')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def form_valid(self, form):
        form.instance.save()
        form.instance.users.add(self.request.user)
        return super().form_valid(form)


class ProjectDeleteView(PermissionRequiredMixin, DeleteView):
    model = Projects
    template_name = 'projects/project_confirm_delete.html'
    success_url = reverse_lazy('projects_list')
    context_object_name = 'project'
    permission_required = 'projects.delete_projects'

    def has_permission(self):
        # Получаем текущий проект
        project = self.get_object()
        # Получаем текущего пользователя
        user = self.request.user
        # Проверяем, есть ли пользователь в списке участников проекта
        return user in project.users.all()


class ProjectUpdateView(PermissionRequiredMixin, UpdateView):
    model = Projects
    template_name = 'projects/project_update.html'
    success_url = reverse_lazy('projects_list')
    form_class = ProjectForm
    permission_required = 'projects.change_projects'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def has_permission(self):
        # Получаем текущий проект
        project = self.get_object()
        # Получаем текущего пользователя
        user = self.request.user
        # Проверяем, есть ли пользователь в списке участников проекта
        return user in project.users.all()


def error_page(request):
    return render(request, 'partials/error_page.html')


class ProjectUserUpdateView(PermissionRequiredMixin, UpdateView):
    model = Projects
    template_name = 'projects/project_update.html'
    success_url = reverse_lazy('projects_list')
    form_class = ProjectUserEditForm
    permission_required = 'projects.change_users_projects'

    def has_permission(self):
        # Получаем текущий проект
        project = self.get_object()
        # Получаем текущего пользователя
        user = self.request.user
        # Проверяем, есть ли пользователь в списке участников проекта
        if user in project.users.all():
            return True
        else:
            # Если пользователь не в списке участников проекта, добавляем сообщение об ошибке
            messages.error(self.request, 'Access denied: You are not a member of this project.')
            return False

    def handle_no_permission(self):
        # Если у пользователя нет разрешения, перенаправляем его на страницу ошибки
        return redirect('error_page')


    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs


class ProjectDetailView(PermissionRequiredMixin, DetailView):
    model = Projects
    template_name = 'projects/project_detail.html'
    context_object_name = 'project'
    success_url = reverse_lazy('projects_list')
    permission_required = 'projects.view_projects'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tasks'] = self.object.tasks.all()
        return context


class ProjectsListView(ListView):
    model = Projects
    template_name = 'projects/projects_list.html'
    context_object_name = 'projects'
    paginate_by = 5
    paginate_orphans = 0

    def get_queryset(self):
        queryset = super().get_queryset()

        search_query = self.request.GET.get('search')
        if search_query:
            queryset = queryset.filter(Q(title__icontains=search_query))

        sort_by = self.request.GET.get('sort_by')
        if sort_by:
            queryset = queryset.order_by(sort_by)
        return queryset

# Create your views here.
