from django import forms
from .models import Task
from django.contrib.auth import get_user_model


class TaskForm(forms.ModelForm):
    author = forms.ModelMultipleChoiceField(
        queryset=get_user_model().objects.all(),
        widget=forms.SelectMultiple(attrs={'class': 'form-select'}),
        required=False
    )

    class Meta:
        model = Task
        fields = ['title', 'description', 'status', 'author', 'project']
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'description': forms.Textarea(attrs={'class': 'form-control'}),
            'status': forms.Select(attrs={'class': 'form-select'}),
            'project': forms.Select(attrs={'class': 'form-select'})
        }

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        instance = super().save(commit=False)
        if commit:
            instance.save()
            if not self.cleaned_data.get('author'):
                instance.author.add(self.user)
            else:
                instance.author.set(self.cleaned_data.get('author'))
        return instance
