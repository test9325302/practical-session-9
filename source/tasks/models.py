from django.contrib.auth import get_user_model
from django.db import models
from projects.models import Projects


class Task(models.Model):
    STATUS_CHOICES = [
        ('in progress', 'In progress'),
        ('done', 'Done'),
        ('is waiting', 'Is waiting'),
    ]

    title = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default='in progress')
    created_at = models.DateTimeField(auto_now_add=True)
    author = models.ManyToManyField(get_user_model(), related_name='tasks', verbose_name='Author')
    project = models.ForeignKey(Projects, on_delete=models.CASCADE, related_name='tasks')

    def __str__(self):
        return self.title

    def to_dict(self):
        return {
            'title': self.title,
            'description': self.description,
            'status': self.status,
            'project': self.project,
            'created_at': self.created_at.isoformat(),
        }
