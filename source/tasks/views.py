from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.shortcuts import render, redirect
from django.views.generic import TemplateView, ListView, DetailView, UpdateView, DeleteView, CreateView
from django.http import JsonResponse
from projects.models import Projects
from .models import Task
from django.urls import reverse_lazy
from .forms import TaskForm


class HomeView(TemplateView):
    template_name = 'home_page.html'

    def get(self, request, *args, **kwargs):
        return render(request, 'home_page.html')


class TaskListView(ListView):
    model = Task
    template_name = 'tasks/task_list.html'
    context_object_name = 'tasks'
    paginate_by = 5
    paginate_orphans = 0

    def get_queryset(self):
        queryset = super().get_queryset()

        search_query = self.request.GET.get('search')
        if search_query:
            queryset = queryset.filter(Q(title__icontains=search_query))

        sort_by = self.request.GET.get('sort_by')
        if sort_by:
            queryset = queryset.order_by(sort_by)
        return queryset


class MyTaskList(TaskListView):
    template_name = 'tasks/my_task_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['user_tasks'] = Task.objects.filter(author=self.request.user)
        return context


class TaskDetailView(DetailView):
    model = Task
    template_name = 'tasks/task_detail.html'
    context_object_name = 'task'


class TaskUpdateView(PermissionRequiredMixin, UpdateView):
    model = Task
    template_name = 'tasks/task_update.html'
    # fields = ['title', 'description', 'status']
    success_url = reverse_lazy('task_list')
    form_class = TaskForm
    permission_required = 'tasks.change_task'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def has_permission(self):
        task = self.get_object()
        user = self.request.user

        return user in task.project.users.all()


class TaskDeleteView(PermissionRequiredMixin, DeleteView):
    model = Task
    template_name = 'tasks/task_confirm_delete.html'
    success_url = reverse_lazy('task_list')
    context_object_name = 'task'
    permission_required = 'tasks.delete_task'

    def has_permission(self):
        task = self.get_object()
        user = self.request.user

        return user in task.project.users.all()


class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    form_class = TaskForm
    template_name = 'tasks/create_task.html'
    success_url = reverse_lazy('task_list')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def form_valid(self, form):
        form.instance.save()
        form.instance.author.add(self.request.user)
        return super().form_valid(form)

    def dispatch(self, request, *args, **kwargs):
        project = request.POST.get('project')
        if project:
            project_instance = Projects.objects.get(id=project)
            if request.user not in project_instance.users.all():
                # raise PermissionDenied("You do not have permission to create an issue in this project.")
                return JsonResponse({'error': "You do not have permission to create an issue in this project."},
                                    status=403)
        return super().dispatch(request, *args, **kwargs)
