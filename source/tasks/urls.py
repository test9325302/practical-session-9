from django.urls import path
from .views import HomeView, TaskListView, TaskDetailView, TaskUpdateView, TaskDeleteView, TaskCreateView, MyTaskList

urlpatterns = [
    path('', HomeView.as_view(), name='home_page'),
    path('list/', TaskListView.as_view(), name='task_list'),
    path('mylist/', MyTaskList.as_view(), name='my_task_list'),
    path('detail/<int:pk>/', TaskDetailView.as_view(), name='task_detail'),
    path('update/<int:pk>/', TaskUpdateView.as_view(), name='task_update'),
    path('delete/<int:pk>/', TaskDeleteView.as_view(), name='task_delete'),
    path('create/', TaskCreateView.as_view(), name='create_task'),

]
