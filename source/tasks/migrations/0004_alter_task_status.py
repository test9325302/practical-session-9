# Generated by Django 5.0.2 on 2024-03-12 12:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0003_remove_task_author_task_author'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='status',
            field=models.CharField(choices=[('in progress', 'In progress'), ('done', 'Done'), ('is waiting', 'Is waiting')], default='в процессе', max_length=20),
        ),
    ]
